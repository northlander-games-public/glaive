import { ViewportModule } from './viewport.module';

describe('ViewportModule', () => {
  let viewportModule: ViewportModule;

  beforeEach(() => {
    viewportModule = new ViewportModule();
  });

  it('should create an instance', () => {
    expect(viewportModule).toBeTruthy();
  });
});
