import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ViewportComponent } from './viewport/viewport.component';
import { Shaders } from './shaders/shaders.service';

@NgModule({
  imports: [
    CommonModule
  ],
  declarations: [
    ViewportComponent
  ],
  providers: [
    Shaders,
  ],
  exports: [
      ViewportComponent
  ]
})
export class ViewportModule { }
