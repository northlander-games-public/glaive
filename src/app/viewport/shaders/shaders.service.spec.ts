import { TestBed } from '@angular/core/testing';

import { Shaders } from './shaders.service';

describe('ShadersService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: ShadersService = TestBed.get(ShadersService);
    expect(service).toBeTruthy();
  });
});
