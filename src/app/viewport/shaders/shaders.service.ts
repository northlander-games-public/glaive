import { Injectable } from '@angular/core';
import { ShaderInfo } from './shaders.source';

@Injectable()
export class Shaders {

	//Shaders doesn't maintain a reference to a context because injectables should be stateless...
  constructor() {}

	//
	// Initialize a shader program, so WebGL knows how to draw our data.
	// @TODO validate input... must be of length (at least?) two, with one vertex shader and one
	// fragment shader.
	//
  initShaderProgram(glContext: WebGLRenderingContext, shadersList: ShaderInfo[]): WebGLProgram | null {
		const glShaders: WebGLShader[] = shadersList.map( curShaderInfo => {
			return this.loadShader(glContext, glContext[curShaderInfo.type], curShaderInfo.source);
		})

    // Create the shader program
    const shaderProgram: WebGLProgram = glContext.createProgram();

    //Attach the (compiled>) shaders to the program and run it through the linker.
    glShaders.forEach( curGLShader => glContext.attachShader(shaderProgram, curGLShader))
    glContext.linkProgram(shaderProgram);

    // If creating the shader program failed, alert

    if (!glContext.getProgramParameter(shaderProgram, glContext.LINK_STATUS)) {
    	//@todo dispatch this error into an error log.
      alert('Unable to initialize the shader program: ' + glContext.getProgramInfoLog(shaderProgram));

      glContext.deleteProgram(shaderProgram);

      return null;
    }

    return shaderProgram;
  }

  //
	// creates a shader of the given type, uploads the source and
	// compiles it.
	//
  loadShader(glContext: WebGLRenderingContext, type: any, source: string): WebGLShader {
	  const shader = glContext.createShader(type);

	  // Send the source to the shader object

	  glContext.shaderSource(shader, source);

	  // Compile the shader program

	  glContext.compileShader(shader);

	  // See if it compiled successfully

	  if (!glContext.getShaderParameter(shader, glContext.COMPILE_STATUS)) {
	    alert('An error occurred compiling the shaders: ' + glContext.getShaderInfoLog(shader));
	    glContext.deleteShader(shader);
	    return null;
	  }

	  return shader;
	}
}