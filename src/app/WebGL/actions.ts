import { Action } from '@ngrx/store';

export enum WebGLActionTypes {
	SET_CONTEXT = 'WebGL.SetContext',
	SET_ERROR = 'WebGL.SetError',
}

export class SetContext implements Action {
	readonly type = WebGLActionTypes.SET_CONTEXT;

	constructor (public payload: WebGLRenderingContext) {}
}

export class SetError implements Action {
	readonly type = WebGLActionTypes.SET_ERROR;

	constructor (public payload: string) {}
}


//WebGLActions - union type for all webgl actions
export type WebGLActions = SetContext | SetError; /* | Next Action | Third Action*/