import { State as WebGLState} from './WebGL/reducer'

export interface AppState {
	webgl: WebGLState
}